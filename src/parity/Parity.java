/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parity;

import java.util.Scanner;

/**
 *
 * @author Skynet
 */
public class Parity {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //hace un while contains para chequear input
        String secuencia;
        String paridad;
        do {
            System.out.print("Ingrese secuencia de bits \n");
            secuencia = new Scanner(System.in).nextLine();
        } while (!secuencia.contains("1") && !secuencia.contains("0"));
        
        do {
            System.out.print("indique paridad de bit (PAR 0 IMPAR 1) \n");
            paridad = new Scanner(System.in).nextLine();
        } while (paridad.charAt(0) != '1' && paridad.charAt(0) != '0');

        if (paridad.equalsIgnoreCase("0")) {
            System.out.println("La secuencia final es " + parityGenEven(secuencia) + "\n");
        } else {
            System.out.println("La secuencia final es " + parityGenOdd(secuencia) + "\n");
        }
    }

    private static String parityGenOdd(String s) {

        int paritycounter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                paritycounter++;
            }
        }
        if (paritycounter % 2 == 0) {
            return s.concat("1");
        } else {
            return s.concat("0");
        }

    }

    private static String parityGenEven(String s) {

        int paritycounter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                paritycounter++;

            }
        }
        if (paritycounter % 2 == 0) {
            return s.concat("0");
        } else {
            return s.concat("1");
        }

    }

}
